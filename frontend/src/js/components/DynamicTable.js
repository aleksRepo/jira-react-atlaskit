import React, { useEffect, useState } from 'react';
import ReactDOM from "react-dom";
import styled from 'styled-components';

import DynamicTable from '@atlaskit/dynamic-table';
import Avatar from "@atlaskit/avatar";
import { AvatarWrapper, NameWrapper } from "./data/sample";

const Wrapper = styled.div`
  min-width: 600px;
`;


export const MyDynamicTable = () => {

    const [data, setData] = useState(null);

    useEffect(() => {
        const data = document.getElementById("data").value
        setData(data)
    }, []);

    if (!data) return (<div>Loading...</div>)

    const head = {
        cells: [
            {
                key: 'name',
                content: 'Name',
                isSortable: true,
                width: 25,
            },
            {
                key: 'assigneetype',
                content: 'Assigneetype',
                shouldTruncate: true,
                isSortable: true,
                width: 15,
            },
            {
                key: 'description',
                content: 'Description',
                shouldTruncate: true,
                isSortable: true,
                width: 10,
            },
            {
                key: 'projecttype',
                content: 'Projecttype',
                shouldTruncate: true,
            },
            {
                key: 'id',
                content: 'Id',
                shouldTruncate: true,
            },
        ]
    };

    const rows = JSON.parse(data).map((data, index) => ({
        key: `row-${index}-${data?.projectGV?.name}`,
        cells: [
            {
                key: 0,
                content: (
                    <NameWrapper>
                        <AvatarWrapper>
                            <Avatar
                                name={data?.projectGV?.name}
                                size="medium"
                            />
                        </AvatarWrapper>
                        <a href="https://atlassian.design">{data?.projectGV?.name}</a>
                    </NameWrapper>
                ),
            },
            {
                key: 1,
                content: data?.projectGV?.assigneetype,
            },
            {
                key: 2,
                content: data?.projectGV?.description,
            },
            {
                key: 3,
                content: data?.projectGV?.projecttype,
            },
            {
                key: 4,
                content: data?.projectGV?.id,
            },
        ],
    }));

    return (
        <Wrapper>
            <DynamicTable
                caption="Lista de proyectos de Jira"
                head={head}
                rows={rows}
                rowsPerPage={10}
                defaultPage={1}
                loadingSpinnerSize="large"
                isRankable
                emptyView={<h2>No hay proyectos en la instancia</h2>}
            />
        </Wrapper>
    );
}


window.addEventListener('load', function () {
    const wrapper = document.getElementById("container");
    wrapper ? ReactDOM.render(<MyDynamicTable/>, wrapper) : false;
});