import React from 'react';
import ReactDOM from "react-dom";
import Button from '@atlaskit/button';
import TextArea from '@atlaskit/textarea';
import TextField from '@atlaskit/textfield';
import axios from 'axios';
import { I18n } from '@atlassian/wrm-react-i18n';

import Form, { Field, FormFooter, FormHeader, FormSection } from '@atlaskit/form';

export const MyFormTwo = () => {

    const onSubmit = async (data) => {
        await axios.post(document.getElementById("contextPath").value + "/plugins/servlet/form", data)
    }

    return (
        <div
            style={{
                display: 'flex',
                width: '400px',
                margin: '0 auto',
                flexDirection: 'column',
            }}
        >
            <Form onSubmit={onSubmit}>
                {({ formProps }) => (
                    <form {...formProps} name="text-fields">
                        <FormHeader
                            title={I18n.getText('form.two.label')}
                            description="* indicates a required field"
                        />
                        <FormSection>
                            <Field name="firstname" defaultValue="" label={I18n.getText('first.name.label')} isRequired>
                                {({ fieldProps }) =>
                                    <TextField {...fieldProps} />}
                            </Field>

                            <Field name="lastname" defaultValue="" label={I18n.getText('last.name.label')} isRequired>
                                {({ fieldProps: { isRequired, isDisabled, ...others } }) => (
                                    <TextField
                                        disabled={isDisabled}
                                        required={isRequired}
                                        {...others}
                                    />
                                )}
                            </Field>
                            <Field
                                name="description"
                                defaultValue=""
                                label="Description"
                            >
                                {({ fieldProps }) =>
                                    <TextArea {...fieldProps} />}
                            </Field>

                            <Field
                                name="comments"
                                defaultValue=""
                                label="Additional comments"
                            >
                                {({ fieldProps }) =>
                                    <TextArea {...fieldProps} />}
                            </Field>
                        </FormSection>
                        <FormFooter>
                            <Button type="submit" appearance="primary">
                                Submit
                            </Button>
                        </FormFooter>
                    </form>
                )}
            </Form>
        </div>

    );
}

window.addEventListener('load', function () {
    const wrapper = document.getElementById("container");
    wrapper ? ReactDOM.render(<MyFormTwo/>, wrapper) : false;
});