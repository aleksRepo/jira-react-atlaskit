package es.suarez.alejandro.atlas.jira.servlet.impl;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import es.suarez.alejandro.atlas.jira.servlet.service.ServletService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

@Component
public class ServletServiceImpl implements ServletService {

    @ComponentImport
    private final LoginUriProvider loginUriProvider;

    public ServletServiceImpl(LoginUriProvider loginUriProvider) {
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    public void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
