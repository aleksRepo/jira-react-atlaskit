package es.suarez.alejandro.atlas.jira.servlet;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.gson.Gson;
import es.suarez.alejandro.atlas.jira.servlet.service.ServletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import es.suarez.alejandro.atlas.jira.service.ResourceService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DynamicTableServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(DynamicTableServlet.class);
    private final ResourceService resourceService;
    private final ServletService servletService;
    private final Gson gson = new Gson();

    @ComponentImport
    private final SoyTemplateRenderer soyTemplateRenderer;
    @ComponentImport
    private final JiraAuthenticationContext jiraAuthenticationContext;
    @ComponentImport
    private final ProjectManager projectManager;

    public DynamicTableServlet(SoyTemplateRenderer soyTemplateRenderer,
                               ResourceService resourceService,
                               ServletService servletService,
                               JiraAuthenticationContext jiraAuthenticationContext,
                               ProjectManager projectManager) {
        this.resourceService = resourceService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.servletService = servletService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectManager = projectManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            servletService.redirectToLogin(req, resp);
        }

        String projects = gson.toJson(projectManager.getProjects());

        String pluginKey = this.resourceService.getProperty("atlassian.plugin.key");
        Map<String, Object> map = new HashMap<>();
        map.put("data",projects);

        String html = soyTemplateRenderer.render(pluginKey + ":jira-react-atlaskit-resources", "servlet.ui.dynamictable", map);

        resp.setContentType("text/html");
        resp.getWriter().write(html);
        resp.getWriter().close();
    }
}