package es.suarez.alejandro.atlas.jira.servlet;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import es.suarez.alejandro.atlas.jira.service.ResourceService;
import es.suarez.alejandro.atlas.jira.servlet.service.ServletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FormTwoServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(FormTwoServlet.class);
    private final ResourceService resourceService;
    private final ServletService servletService;

    @ComponentImport
    private final SoyTemplateRenderer soyTemplateRenderer;
    @ComponentImport
    private final JiraAuthenticationContext jiraAuthenticationContext;


    public FormTwoServlet(SoyTemplateRenderer soyTemplateRenderer,
                          ResourceService resourceService,
                          ServletService servletService,
                          JiraAuthenticationContext jiraAuthenticationContext) {
        this.resourceService = resourceService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.servletService = servletService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            servletService.redirectToLogin(req, resp);
        }

        render(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            servletService.redirectToLogin(req, resp);
        }

        StringBuilder data = new StringBuilder();
        String line;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null)
                data.append(line);
        } catch (Exception ignored) {}
        log.warn(String.format("Post Data: %s", data));

        render(req, resp);
    }

    private void render(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String pluginKey = this.resourceService.getProperty("atlassian.plugin.key");
        Map<String, Object> map = new HashMap<>();
        map.put("contextPath", req.getContextPath());
        map.put("pathtoswaggerjson", "/jira/download/resources/ru.matveev.alexey.atlas.jira.backend:jira-react-atlaskit-resources/swagger/swagger.v3.json");
        String html = soyTemplateRenderer.render(pluginKey + ":jira-react-atlaskit-resources", "servlet.ui.form_two", map);

        resp.setContentType("text/html");
        resp.getWriter().write(html);
        resp.getWriter().close();
    }
}